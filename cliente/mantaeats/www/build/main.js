webpackJsonp([8],{

/***/ 101:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LocalesService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_fire_database__ = __webpack_require__(154);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_fire__ = __webpack_require__(57);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_angular__ = __webpack_require__(21);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var LocalesService = /** @class */ (function () {
    function LocalesService(afDB, firebase, alertCtrl) {
        this.afDB = afDB;
        this.firebase = firebase;
        this.alertCtrl = alertCtrl;
    }
    LocalesService.prototype.getLocales = function () {
        return this.afDB.list('/locales/');
    };
    LocalesService.prototype.getLocal = function (id) {
        return this.afDB.object('/locales/' + id);
    };
    LocalesService.prototype.createLocal = function (local) {
        return this.afDB.database.ref('/locales/' + local.id).set(local);
    };
    LocalesService.prototype.editLocal = function (local) {
        return this.afDB.database.ref('/locales/' + local.id).set(local);
    };
    LocalesService.prototype.deleteLocal = function (local) {
        var _this = this;
        return this.afDB.database.ref('/locales/' + local.id).remove().then(function (result) {
            var alert = _this.alertCtrl.create({
                title: 'Operación exitosa',
                subTitle: 'El local fue eliminado correctamente.',
                buttons: ['Aceptar']
            });
            alert.present();
        });
    };
    LocalesService.prototype.imagenABase64 = function (img, callback) {
        var fileReader = new FileReader();
        fileReader.addEventListener('load', function (event) {
            callback(fileReader.result);
        });
        fileReader.readAsDataURL(img);
    };
    LocalesService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_fire_database__["a" /* AngularFireDatabase */], __WEBPACK_IMPORTED_MODULE_2__angular_fire__["b" /* FirebaseApp */], __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["a" /* AlertController */]])
    ], LocalesService);
    return LocalesService;
}());

//# sourceMappingURL=locales.service.js.map

/***/ }),

/***/ 170:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AuthService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_fire_database__ = __webpack_require__(154);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_fire_auth__ = __webpack_require__(296);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_firebase_app__ = __webpack_require__(284);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_firebase_app___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_firebase_app__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var AuthService = /** @class */ (function () {
    function AuthService(afDB, afAuth) {
        this.afDB = afDB;
        this.afAuth = afAuth;
    }
    AuthService.prototype.loginConFacebook = function () {
        return this.afAuth.auth.signInWithPopup(new __WEBPACK_IMPORTED_MODULE_3_firebase_app__["auth"].FacebookAuthProvider());
    };
    AuthService.prototype.logout = function () {
        return this.afAuth.auth.signOut();
    };
    AuthService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_fire_database__["a" /* AngularFireDatabase */], __WEBPACK_IMPORTED_MODULE_2__angular_fire_auth__["a" /* AngularFireAuth */]])
    ], AuthService);
    return AuthService;
}());

//# sourceMappingURL=auth.service.js.map

/***/ }),

/***/ 193:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ModalContentLocalEditPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_locales_service__ = __webpack_require__(101);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/**
 * Generated class for the ModalContentLocalEditPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var ModalContentLocalEditPage = /** @class */ (function () {
    function ModalContentLocalEditPage(navCtrl, navParams, localesService, alertCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.localesService = localesService;
        this.alertCtrl = alertCtrl;
        this.local = {};
        this.local = navParams.get('local');
    }
    ModalContentLocalEditPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ModalContentLocalEditPage');
    };
    ModalContentLocalEditPage.prototype.modificarLocal = function (local) {
        if (!local.id) {
            local.id = Date.now();
        }
        if (String(local.nombre).trim().charAt(0) == "" || String(local.direccion).trim().charAt(0) == "" || String(local.likes).trim().charAt(0) == "" || String(local.followers).trim().charAt(0) == "") {
            var alert_1 = this.alertCtrl.create({
                title: 'No se modificó el local',
                subTitle: 'Por favor, rellena todos los campos.',
                buttons: ['Entiendo']
            });
            alert_1.present();
        }
        else {
            this.localesService.createLocal(local);
            console.log(local);
            var alert_2 = this.alertCtrl.create({
                title: 'Operación realizada con éxito',
                subTitle: 'Se modificó el local correctamente.',
                buttons: ['Aceptar']
            });
            alert_2.present();
            this.navCtrl.pop();
        }
    };
    ModalContentLocalEditPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-modal-content-local-edit',template:/*ion-inline-start:"C:\Users\Jesucristhorr\Documents\proyectoProgramacionWeb\cliente\mantaeats\src\pages\modal-content-local-edit\modal-content-local-edit.html"*/'<!--\n  Generated template for the ModalContentLocalPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n    <ion-title text-center>Modifique un local existente</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n  <ion-grid>\n    <ion-row>\n      <ion-col col-30>\n        <ion-item color="inputs">\n          <ion-label floating color="inputLabel" style="font-weight:bold">Nombre del local</ion-label>\n          <ion-input type="text" style="color:white" [(ngModel)]="local.nombre"></ion-input>\n        </ion-item>\n\n        <ion-item color="inputs">\n          <ion-label floating color="inputLabel" style="font-weight:bold">Dirección del local</ion-label>\n          <ion-input type="text" style="color:white" [(ngModel)]="local.direccion"></ion-input>\n        </ion-item>\n\n        <ion-item color="inputs">\n          <ion-label floating color="inputLabel" style="font-weight:bold">Likes en Facebook</ion-label>\n          <ion-input type="number" step="1" style="color:white" [(ngModel)]="local.likes"></ion-input>\n        </ion-item>\n\n        <ion-item color="inputs">\n          <ion-label floating color="inputLabel" style="font-weight:bold">Seguidores en Twitter</ion-label>\n          <ion-input type="number" step="1" style="color:white" [(ngModel)]="local.followers"></ion-input>\n        </ion-item>\n\n        <ion-item color="inputs" text-center>\n          <button ion-button (click)="modificarLocal(local)" medium>Modificar local existente</button>\n        </ion-item>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n    \n</ion-content>\n'/*ion-inline-end:"C:\Users\Jesucristhorr\Documents\proyectoProgramacionWeb\cliente\mantaeats\src\pages\modal-content-local-edit\modal-content-local-edit.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__services_locales_service__["a" /* LocalesService */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */]])
    ], ModalContentLocalEditPage);
    return ModalContentLocalEditPage;
}());

//# sourceMappingURL=modal-content-local-edit.js.map

/***/ }),

/***/ 194:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ModalContentLocalPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_locales_service__ = __webpack_require__(101);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_fire__ = __webpack_require__(57);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/**
 * Generated class for the ModalContentLocalPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var ModalContentLocalPage = /** @class */ (function () {
    function ModalContentLocalPage(navCtrl, navParams, viewCtrl, localesService, firebase, alertCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.viewCtrl = viewCtrl;
        this.localesService = localesService;
        this.firebase = firebase;
        this.alertCtrl = alertCtrl;
        this.local = {};
        this.local = navParams.get('local');
    }
    ModalContentLocalPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ModalContentLocalPage');
    };
    ModalContentLocalPage.prototype.changeListener = function ($event) {
        this.file = $event.target.files[0];
    };
    ModalContentLocalPage.prototype.subirImagenAFirebase = function (imagen, local) {
        var _this = this;
        console.log(local);
        if (imagen == null || String(local.nombre).trim().charAt(0) == "" || String(local.direccion).trim().charAt(0) == "" || String(local.likes).trim().charAt(0) == "" || String(local.followers).trim().charAt(0) == "") {
            var alert_1 = this.alertCtrl.create({
                title: 'No se agregó el local',
                subTitle: 'Por favor, rellena todos los campos.',
                buttons: ['Entiendo']
            });
            alert_1.present();
        }
        else {
            local.id = Date.now();
            this.localesService.createLocal(local);
            var storageRef_1 = this.firebase.storage().ref('/locales/' + local.id);
            this.localesService.imagenABase64(imagen, function (base64) {
                storageRef_1.putString(base64, 'data_url');
            });
            var alert_2 = this.alertCtrl.create({
                title: 'Se agregó el local correctamente',
                subTitle: 'Gracias por agregar un nuevo local.',
                buttons: ['Aceptar']
            });
            alert_2.present();
            setTimeout(function () { return _this.navCtrl.pop(); }, 2000);
        }
    };
    ModalContentLocalPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-modal-content-local',template:/*ion-inline-start:"C:\Users\Jesucristhorr\Documents\proyectoProgramacionWeb\cliente\mantaeats\src\pages\modal-content-local\modal-content-local.html"*/'<!--\n  Generated template for the ModalContentLocalPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n    <ion-title text-center>Agregue un nuevo local</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n  <ion-grid>\n    <ion-row>\n      <ion-col col-30>\n        <ion-item color="inputs">\n          <ion-label floating color="inputLabel" style="font-weight:bold">Nombre del local</ion-label>\n          <ion-input type="text" style="color:white" [(ngModel)]="local.nombre" value=" "></ion-input>\n        </ion-item>\n\n        <ion-item color="inputs">\n          <ion-label floating color="inputLabel" style="font-weight:bold">Dirección del local</ion-label>\n          <ion-input type="text" style="color:white" [(ngModel)]="local.direccion" value=" "></ion-input>\n        </ion-item>\n\n        <ion-item color="inputs">\n          <ion-label floating color="inputLabel" style="font-weight:bold">Likes en Facebook</ion-label>\n          <ion-input type="number" step="1" style="color:white" [(ngModel)]="local.likes" value=" "></ion-input>\n        </ion-item>\n\n        <ion-item color="inputs">\n          <ion-label floating color="inputLabel" style="font-weight:bold">Seguidores en Twitter</ion-label>\n          <ion-input type="number" step="1" style="color:white" [(ngModel)]="local.followers" value=" "></ion-input>\n        </ion-item>\n\n        <ion-item color="inputs">\n          <ion-label stacked color="inputLabel" style="font-weight:bold">Imagen del local</ion-label>\n          <ion-input type="file" accept="image/*" (change)="changeListener($event)" style="color:white" [(ngModel)]="local.imagen"></ion-input>\n        </ion-item>\n        <ion-item color="inputs" text-center>\n          <button ion-button (click)="subirImagenAFirebase(file, local)" medium>Agregar nuevo local</button>\n        </ion-item>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n    \n</ion-content>\n'/*ion-inline-end:"C:\Users\Jesucristhorr\Documents\proyectoProgramacionWeb\cliente\mantaeats\src\pages\modal-content-local\modal-content-local.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* ViewController */], __WEBPACK_IMPORTED_MODULE_2__services_locales_service__["a" /* LocalesService */], __WEBPACK_IMPORTED_MODULE_3__angular_fire__["b" /* FirebaseApp */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */]])
    ], ModalContentLocalPage);
    return ModalContentLocalPage;
}());

//# sourceMappingURL=modal-content-local.js.map

/***/ }),

/***/ 195:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ModalContentPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(21);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the ModalContentPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var ModalContentPage = /** @class */ (function () {
    function ModalContentPage(navCtrl, navParams, viewCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.viewCtrl = viewCtrl;
    }
    ModalContentPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ModalContentPage');
    };
    ModalContentPage.prototype.closeModal = function () {
        this.viewCtrl.dismiss();
    };
    ModalContentPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-modal-content',template:/*ion-inline-start:"C:\Users\Jesucristhorr\Documents\proyectoProgramacionWeb\cliente\mantaeats\src\pages\modal-content\modal-content.html"*/'<!--\n  Generated template for the ModalContentPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n    <ion-title text-center>Realice su pedido en el local</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\nhello\n</ion-content>\n'/*ion-inline-end:"C:\Users\Jesucristhorr\Documents\proyectoProgramacionWeb\cliente\mantaeats\src\pages\modal-content\modal-content.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* ViewController */]])
    ], ModalContentPage);
    return ModalContentPage;
}());

//# sourceMappingURL=modal-content.js.map

/***/ }),

/***/ 196:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LocalesPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__modal_content_modal_content__ = __webpack_require__(195);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__modal_content_local_modal_content_local__ = __webpack_require__(194);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__services_locales_service__ = __webpack_require__(101);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__modal_content_local_edit_modal_content_local_edit__ = __webpack_require__(193);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__angular_fire__ = __webpack_require__(57);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__login_login__ = __webpack_require__(85);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__perfil_perfil__ = __webpack_require__(86);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









/**
 * Generated class for the LocalesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var LocalesPage = /** @class */ (function () {
    function LocalesPage(navCtrl, navParams, loadingCtrl, modalCtrl, alertCtrl, localesService, firebase) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.loadingCtrl = loadingCtrl;
        this.modalCtrl = modalCtrl;
        this.alertCtrl = alertCtrl;
        this.localesService = localesService;
        this.firebase = firebase;
        this.locales = [];
        this.verificarSesion();
        this.localesService.getLocales().valueChanges().subscribe(function (localesFB) {
            _this.locales = localesFB;
        });
    }
    LocalesPage.prototype.ionViewWillEnter = function () {
        this.verificarSesion();
    };
    LocalesPage.prototype.realizarPedido = function () {
        if (localStorage.getItem('loginData') != null) {
            this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__modal_content_modal_content__["a" /* ModalContentPage */], { local: {} });
        }
        else {
            var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_7__login_login__["a" /* LoginPage */]);
            modal.present();
        }
    };
    LocalesPage.prototype.irAgregarLocal = function () {
        if (localStorage.getItem('loginData') != null) {
            if (JSON.parse(localStorage.loginData).user.uid == 'Ks0SVJI1QSg2m9ra1x1AsQkfpUl2') {
                this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__modal_content_local_modal_content_local__["a" /* ModalContentLocalPage */], { local: {} });
            }
            else {
                var alert_1 = this.alertCtrl.create({
                    title: 'Lo sentimos',
                    subTitle: 'No posees los permisos necesarios para agregar locales, contacta al administrador.',
                    buttons: ['Entiendo']
                });
                alert_1.present();
            }
        }
        else {
            var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_7__login_login__["a" /* LoginPage */]);
            modal.present();
        }
    };
    LocalesPage.prototype.irEditarLocal = function (local) {
        if (localStorage.getItem('loginData') != null) {
            if (JSON.parse(localStorage.loginData).user.uid == 'Ks0SVJI1QSg2m9ra1x1AsQkfpUl2') {
                this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_5__modal_content_local_edit_modal_content_local_edit__["a" /* ModalContentLocalEditPage */], { local: local });
            }
            else {
                var alert_2 = this.alertCtrl.create({
                    title: 'Lo sentimos',
                    subTitle: 'No posees los permisos necesarios para editar locales, contacta al administrador.',
                    buttons: ['Entiendo']
                });
                alert_2.present();
            }
        }
        else {
            var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_7__login_login__["a" /* LoginPage */]);
            modal.present();
        }
    };
    LocalesPage.prototype.eliminarLocal = function (local) {
        var _this = this;
        if (localStorage.getItem('loginData') != null) {
            if (JSON.parse(localStorage.loginData).user.uid == 'Ks0SVJI1QSg2m9ra1x1AsQkfpUl2') {
                var confirm_1 = this.alertCtrl.create({
                    title: 'Confirmación',
                    message: '¿Estás seguro de que deseas eliminar este local?',
                    buttons: [
                        {
                            text: 'Cancelar',
                            handler: function () {
                                console.log('Cancelar clicked');
                            }
                        },
                        {
                            text: 'Aceptar',
                            handler: function () {
                                console.log('Aceptar clicked');
                                _this.firebase.storage().ref('/locales/' + local.id).delete();
                                _this.localesService.deleteLocal(local);
                            }
                        }
                    ]
                });
                confirm_1.present();
            }
            else {
                var alert_3 = this.alertCtrl.create({
                    title: 'Lo sentimos',
                    subTitle: 'No posees los permisos necesarios para eliminar locales, contacta al administrador.',
                    buttons: ['Entiendo']
                });
                alert_3.present();
            }
        }
        else {
            var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_7__login_login__["a" /* LoginPage */]);
            modal.present();
        }
    };
    LocalesPage.prototype.verificarSesion = function () {
        var _this = this;
        setTimeout(function () {
            _this.imagen = document.getElementById('imagenLocal');
            if (localStorage.getItem('loginData') != null) {
                var rutaImagen = JSON.parse(localStorage.loginData).user.photoURL;
                _this.imagen.setAttribute("src", rutaImagen);
            }
            else {
                _this.imagen.setAttribute("src", "http://cdn.onlinewebfonts.com/svg/img_411680.png");
            }
        }, 1);
    };
    LocalesPage.prototype.tocarImagen = function () {
        if (localStorage.getItem('loginData') != null) {
            this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_8__perfil_perfil__["a" /* PerfilPage */]);
        }
        else {
            this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_7__login_login__["a" /* LoginPage */]);
        }
    };
    LocalesPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-locales',template:/*ion-inline-start:"C:\Users\Jesucristhorr\Documents\proyectoProgramacionWeb\cliente\mantaeats\src\pages\locales\locales.html"*/'<ion-header>\n  <ion-navbar color="danger">\n    <ion-buttons start>\n      <button ion-button menuToggle icon-only>\n        <ion-icon name=\'information-circle\'></ion-icon>\n      </button>\n    </ion-buttons>\n    <ion-title>Locales</ion-title>\n    <ion-buttons end>\n      <button ion-button small clear (click)="tocarImagen()">\n        <img height="30" width="30" src="" id="imagenLocal" class="imagenPerfil">\n      </button>\n    </ion-buttons>\n  </ion-navbar>\n</ion-header>\n\n<ion-content padding>\n  <ion-card *ngFor="let local of locales">\n    <img src="https://firebasestorage.googleapis.com/v0/b/manta-eats.appspot.com/o/locales%2F{{local.id}}?alt=media&token=334bdd2c-fd82-444b-8e2a-f49d99991388">\n    <ion-card-content>\n      <ion-card-title style = "color: white" text-center>\n        {{local.nombre}}\n      </ion-card-title>\n      <p style="color: rgb(172, 172, 172)" text-center>{{local.direccion}}</p>\n    </ion-card-content>\n\n    <ion-item>\n      <ion-icon name="md-thumbs-up" item-start style = "color: #48659A"></ion-icon>\n      Me gusta\n      <ion-badge item-end>{{local.likes}}</ion-badge>\n    </ion-item>\n\n    <ion-item>\n      <ion-icon name=\'logo-twitter\' item-start style="color: #55acee"></ion-icon>\n      Seguidores\n      <ion-badge item-end>{{local.followers}}</ion-badge>\n    </ion-item>\n\n    <ion-item>\n        <button ion-button clear (click)="realizarPedido()" large item-start><ion-icon name="cart"></ion-icon><p padding style="color: #e0b938">Realizar pedido</p></button>\n        <button ion-button clear (click)="irEditarLocal(local)" large item-end><ion-icon name="create"></ion-icon><p padding style="color: #e0b938">Editar</p></button>\n    </ion-item>\n\n    <ion-item text-center>\n        <button ion-button clear (click)="eliminarLocal(local)" large><ion-icon name="trash" style="color: red"></ion-icon><p padding style="color: red">Eliminar local</p></button>\n    </ion-item>\n  </ion-card>\n\n  <ion-fab bottom right>\n    <button ion-fab color="danger" (click)="irAgregarLocal()"><ion-icon name="add"></ion-icon></button>\n  </ion-fab>\n</ion-content>\n'/*ion-inline-end:"C:\Users\Jesucristhorr\Documents\proyectoProgramacionWeb\cliente\mantaeats\src\pages\locales\locales.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* ModalController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */], __WEBPACK_IMPORTED_MODULE_4__services_locales_service__["a" /* LocalesService */], __WEBPACK_IMPORTED_MODULE_6__angular_fire__["b" /* FirebaseApp */]])
    ], LocalesPage);
    return LocalesPage;
}());

//# sourceMappingURL=locales.js.map

/***/ }),

/***/ 197:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TabsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__home_home__ = __webpack_require__(298);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__locales_locales__ = __webpack_require__(196);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__perfil_perfil__ = __webpack_require__(86);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





/**
 * Generated class for the TabsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var TabsPage = /** @class */ (function () {
    function TabsPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.tab1Root = __WEBPACK_IMPORTED_MODULE_2__home_home__["a" /* HomePage */];
        this.tab2Root = __WEBPACK_IMPORTED_MODULE_3__locales_locales__["a" /* LocalesPage */];
        this.tab3Root = __WEBPACK_IMPORTED_MODULE_4__perfil_perfil__["a" /* PerfilPage */];
    }
    TabsPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad TabsPage');
    };
    TabsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-tabs',template:/*ion-inline-start:"C:\Users\Jesucristhorr\Documents\proyectoProgramacionWeb\cliente\mantaeats\src\pages\tabs\tabs.html"*/'<ion-tabs color="light">\n  <ion-tab tabTitle="Inicio" tabIcon="home" [root]="tab1Root"></ion-tab>\n  <ion-tab tabTitle="Locales" tabIcon="pizza" [root]="tab2Root"></ion-tab>\n  <ion-tab tabTitle="Perfil" tabIcon="cog" [root]="tab3Root"></ion-tab>\n</ion-tabs>'/*ion-inline-end:"C:\Users\Jesucristhorr\Documents\proyectoProgramacionWeb\cliente\mantaeats\src\pages\tabs\tabs.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */]])
    ], TabsPage);
    return TabsPage;
}());

//# sourceMappingURL=tabs.js.map

/***/ }),

/***/ 236:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 236;

/***/ }),

/***/ 277:
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"../pages/info/info.module": [
		591,
		7
	],
	"../pages/locales/locales.module": [
		596,
		6
	],
	"../pages/login/login.module": [
		593,
		5
	],
	"../pages/modal-content-local-edit/modal-content-local-edit.module": [
		592,
		4
	],
	"../pages/modal-content-local/modal-content-local.module": [
		594,
		3
	],
	"../pages/modal-content/modal-content.module": [
		595,
		2
	],
	"../pages/perfil/perfil.module": [
		598,
		1
	],
	"../pages/tabs/tabs.module": [
		597,
		0
	]
};
function webpackAsyncContext(req) {
	var ids = map[req];
	if(!ids)
		return Promise.reject(new Error("Cannot find module '" + req + "'."));
	return __webpack_require__.e(ids[1]).then(function() {
		return __webpack_require__(ids[0]);
	});
};
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
webpackAsyncContext.id = 277;
module.exports = webpackAsyncContext;

/***/ }),

/***/ 298:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HomePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__login_login__ = __webpack_require__(85);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__perfil_perfil__ = __webpack_require__(86);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var HomePage = /** @class */ (function () {
    function HomePage(navCtrl, viewCtrl) {
        this.navCtrl = navCtrl;
        this.viewCtrl = viewCtrl;
        this.verificarSesion();
    }
    HomePage.prototype.ionViewWillEnter = function () {
        this.verificarSesion();
    };
    HomePage.prototype.verificarSesion = function () {
        var _this = this;
        setTimeout(function () {
            _this.imagen = document.getElementById('imagen');
            if (localStorage.getItem('loginData') != null) {
                var rutaImagen = JSON.parse(localStorage.loginData).user.photoURL;
                _this.imagen.setAttribute("src", rutaImagen);
            }
            else {
                _this.imagen.setAttribute("src", "http://cdn.onlinewebfonts.com/svg/img_411680.png");
            }
        }, 1);
    };
    HomePage.prototype.tocarImagen = function () {
        if (localStorage.getItem('loginData') != null) {
            this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__perfil_perfil__["a" /* PerfilPage */]);
        }
        else {
            this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__login_login__["a" /* LoginPage */]);
        }
    };
    HomePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-home',template:/*ion-inline-start:"C:\Users\Jesucristhorr\Documents\proyectoProgramacionWeb\cliente\mantaeats\src\pages\home\home.html"*/'<ion-header>\n  <ion-navbar color="danger">\n    <ion-buttons start>\n      <button ion-button menuToggle icon-only start>\n        <ion-icon name=\'information-circle\'></ion-icon>\n      </button>\n    </ion-buttons>\n    <ion-title text-center>Inicio</ion-title>\n    <ion-buttons end>\n      <button ion-button small clear end (click)="tocarImagen()">\n        <img height="30" width="30" src="" id="imagen" class="imagenPerfil">\n      </button>\n    </ion-buttons>\n  </ion-navbar>\n</ion-header>\n\n<ion-content padding>\n  <p text-center>¡Bienvenido a Manta Eats!</p>\n</ion-content>'/*ion-inline-end:"C:\Users\Jesucristhorr\Documents\proyectoProgramacionWeb\cliente\mantaeats\src\pages\home\home.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* ViewController */]])
    ], HomePage);
    return HomePage;
}());

//# sourceMappingURL=home.js.map

/***/ }),

/***/ 342:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return InfoPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(21);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the InfoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var InfoPage = /** @class */ (function () {
    function InfoPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    InfoPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad InfoPage');
    };
    InfoPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-info',template:/*ion-inline-start:"C:\Users\Jesucristhorr\Documents\proyectoProgramacionWeb\cliente\mantaeats\src\pages\info\info.html"*/'<!--\n  Generated template for the InfoPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n    <ion-title>info</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n\n</ion-content>\n'/*ion-inline-end:"C:\Users\Jesucristhorr\Documents\proyectoProgramacionWeb\cliente\mantaeats\src\pages\info\info.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */]])
    ], InfoPage);
    return InfoPage;
}());

//# sourceMappingURL=info.js.map

/***/ }),

/***/ 343:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__(344);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_module__ = __webpack_require__(476);


Object(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_1__app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 476:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export firebaseConfig */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__(55);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_component__ = __webpack_require__(580);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_home_home__ = __webpack_require__(298);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__pages_list_list__ = __webpack_require__(588);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_native_status_bar__ = __webpack_require__(338);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__ionic_native_splash_screen__ = __webpack_require__(341);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__pages_locales_locales__ = __webpack_require__(196);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__pages_info_info__ = __webpack_require__(342);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__pages_perfil_perfil__ = __webpack_require__(86);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__pages_tabs_tabs__ = __webpack_require__(197);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__angular_fire__ = __webpack_require__(57);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__angular_fire_database__ = __webpack_require__(154);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__angular_fire_auth__ = __webpack_require__(296);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__pages_modal_content_modal_content__ = __webpack_require__(195);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__pages_modal_content_local_modal_content_local__ = __webpack_require__(194);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__services_locales_service__ = __webpack_require__(101);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18_firebase_storage__ = __webpack_require__(589);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__pages_modal_content_local_edit_modal_content_local_edit__ = __webpack_require__(193);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__pages_login_login__ = __webpack_require__(85);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_21__services_auth_service__ = __webpack_require__(170);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






















var firebaseConfig = {
    apiKey: "AIzaSyA3yK7eQeFwJZLfqcRAkgKOSABiQ5ESUaw",
    authDomain: "manta-eats.firebaseapp.com",
    databaseURL: "https://manta-eats.firebaseio.com",
    projectId: "manta-eats",
    storageBucket: "manta-eats.appspot.com",
    messagingSenderId: "746698395002"
};
var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_3__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_4__pages_home_home__["a" /* HomePage */],
                __WEBPACK_IMPORTED_MODULE_5__pages_list_list__["a" /* ListPage */],
                __WEBPACK_IMPORTED_MODULE_8__pages_locales_locales__["a" /* LocalesPage */],
                __WEBPACK_IMPORTED_MODULE_9__pages_info_info__["a" /* InfoPage */],
                __WEBPACK_IMPORTED_MODULE_10__pages_perfil_perfil__["a" /* PerfilPage */],
                __WEBPACK_IMPORTED_MODULE_11__pages_tabs_tabs__["a" /* TabsPage */],
                __WEBPACK_IMPORTED_MODULE_15__pages_modal_content_modal_content__["a" /* ModalContentPage */],
                __WEBPACK_IMPORTED_MODULE_16__pages_modal_content_local_modal_content_local__["a" /* ModalContentLocalPage */],
                __WEBPACK_IMPORTED_MODULE_19__pages_modal_content_local_edit_modal_content_local_edit__["a" /* ModalContentLocalEditPage */],
                __WEBPACK_IMPORTED_MODULE_20__pages_login_login__["a" /* LoginPage */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["d" /* IonicModule */].forRoot(__WEBPACK_IMPORTED_MODULE_3__app_component__["a" /* MyApp */], {}, {
                    links: [
                        { loadChildren: '../pages/info/info.module#InfoPageModule', name: 'InfoPage', segment: 'info', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/modal-content-local-edit/modal-content-local-edit.module#ModalContentLocalEditPageModule', name: 'ModalContentLocalEditPage', segment: 'modal-content-local-edit', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/login/login.module#LoginPageModule', name: 'LoginPage', segment: 'login', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/modal-content-local/modal-content-local.module#ModalContentLocalPageModule', name: 'ModalContentLocalPage', segment: 'modal-content-local', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/modal-content/modal-content.module#ModalContentPageModule', name: 'ModalContentPage', segment: 'modal-content', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/locales/locales.module#LocalesPageModule', name: 'LocalesPage', segment: 'locales', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/tabs/tabs.module#TabsPageModule', name: 'TabsPage', segment: 'tabs', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/perfil/perfil.module#PerfilPageModule', name: 'PerfilPage', segment: 'perfil', priority: 'low', defaultHistory: [] }
                    ]
                }),
                __WEBPACK_IMPORTED_MODULE_12__angular_fire__["a" /* AngularFireModule */].initializeApp(firebaseConfig),
                __WEBPACK_IMPORTED_MODULE_13__angular_fire_database__["b" /* AngularFireDatabaseModule */],
                __WEBPACK_IMPORTED_MODULE_14__angular_fire_auth__["b" /* AngularFireAuthModule */]
            ],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["b" /* IonicApp */]],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_3__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_4__pages_home_home__["a" /* HomePage */],
                __WEBPACK_IMPORTED_MODULE_5__pages_list_list__["a" /* ListPage */],
                __WEBPACK_IMPORTED_MODULE_8__pages_locales_locales__["a" /* LocalesPage */],
                __WEBPACK_IMPORTED_MODULE_9__pages_info_info__["a" /* InfoPage */],
                __WEBPACK_IMPORTED_MODULE_10__pages_perfil_perfil__["a" /* PerfilPage */],
                __WEBPACK_IMPORTED_MODULE_11__pages_tabs_tabs__["a" /* TabsPage */],
                __WEBPACK_IMPORTED_MODULE_15__pages_modal_content_modal_content__["a" /* ModalContentPage */],
                __WEBPACK_IMPORTED_MODULE_16__pages_modal_content_local_modal_content_local__["a" /* ModalContentLocalPage */],
                __WEBPACK_IMPORTED_MODULE_19__pages_modal_content_local_edit_modal_content_local_edit__["a" /* ModalContentLocalEditPage */],
                __WEBPACK_IMPORTED_MODULE_20__pages_login_login__["a" /* LoginPage */]
            ],
            providers: [
                __WEBPACK_IMPORTED_MODULE_6__ionic_native_status_bar__["a" /* StatusBar */],
                __WEBPACK_IMPORTED_MODULE_7__ionic_native_splash_screen__["a" /* SplashScreen */],
                { provide: __WEBPACK_IMPORTED_MODULE_1__angular_core__["u" /* ErrorHandler */], useClass: __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["c" /* IonicErrorHandler */] },
                __WEBPACK_IMPORTED_MODULE_17__services_locales_service__["a" /* LocalesService */],
                __WEBPACK_IMPORTED_MODULE_21__services_auth_service__["a" /* AuthService */]
            ],
        })
    ], AppModule);
    return AppModule;
}());

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 580:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyApp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__ = __webpack_require__(338);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(341);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_tabs_tabs__ = __webpack_require__(197);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var MyApp = /** @class */ (function () {
    function MyApp(platform, statusBar, splashScreen) {
        this.platform = platform;
        this.statusBar = statusBar;
        this.splashScreen = splashScreen;
        this.rootPage = __WEBPACK_IMPORTED_MODULE_4__pages_tabs_tabs__["a" /* TabsPage */];
        this.initializeApp();
        // used for an example of ngFor and navigation
    }
    MyApp.prototype.initializeApp = function () {
        var _this = this;
        this.platform.ready().then(function () {
            // Okay, so the platform is ready and our plugins are available.
            // Here you can do any higher level native things you might need.
            _this.statusBar.styleDefault();
            _this.splashScreen.hide();
        });
    };
    MyApp.prototype.openPage = function (page) {
        // Reset the content nav to have just this page
        // we wouldn't want the back button to show in this scenario
        this.nav.setRoot(page.component);
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* Nav */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* Nav */])
    ], MyApp.prototype, "nav", void 0);
    MyApp = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({template:/*ion-inline-start:"C:\Users\Jesucristhorr\Documents\proyectoProgramacionWeb\cliente\mantaeats\src\app\app.html"*/'<ion-menu [content]="content">\n  <ion-header>\n    <ion-toolbar color="danger">\n      <ion-title>Acerca de</ion-title>\n    </ion-toolbar>\n  </ion-header>\n\n  <ion-content>\n  </ion-content>\n\n</ion-menu>\n\n<!-- Disable swipe-to-go-back because it\'s poor UX to combine STGB with side menus -->\n<ion-nav [root]="rootPage" #content swipeBackEnabled="false"></ion-nav>'/*ion-inline-end:"C:\Users\Jesucristhorr\Documents\proyectoProgramacionWeb\cliente\mantaeats\src\app\app.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* Platform */], __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__["a" /* StatusBar */], __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */]])
    ], MyApp);
    return MyApp;
}());

//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ 588:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ListPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(21);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ListPage = /** @class */ (function () {
    function ListPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        // If we navigated to this page, we will have an item available as a nav param
        this.selectedItem = navParams.get('item');
        // Let's populate this page with some filler content for funzies
        this.icons = ['flask', 'wifi', 'beer', 'football', 'basketball', 'paper-plane',
            'american-football', 'boat', 'bluetooth', 'build'];
        this.items = [];
        for (var i = 1; i < 11; i++) {
            this.items.push({
                title: 'Item ' + i,
                note: 'This is item #' + i,
                icon: this.icons[Math.floor(Math.random() * this.icons.length)]
            });
        }
    }
    ListPage_1 = ListPage;
    ListPage.prototype.itemTapped = function (event, item) {
        // That's right, we're pushing to ourselves!
        this.navCtrl.push(ListPage_1, {
            item: item
        });
    };
    ListPage = ListPage_1 = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-list',template:/*ion-inline-start:"C:\Users\Jesucristhorr\Documents\proyectoProgramacionWeb\cliente\mantaeats\src\pages\list\list.html"*/'<ion-header>\n  <ion-navbar>\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title>List</ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content>\n  <ion-list>\n    <button ion-item *ngFor="let item of items" (click)="itemTapped($event, item)">\n      <ion-icon [name]="item.icon" item-start></ion-icon>\n      {{item.title}}\n      <div class="item-note" item-end>\n        {{item.note}}\n      </div>\n    </button>\n  </ion-list>\n  <div *ngIf="selectedItem" padding>\n    You navigated here from <b>{{selectedItem.title}}</b>\n  </div>\n</ion-content>\n'/*ion-inline-end:"C:\Users\Jesucristhorr\Documents\proyectoProgramacionWeb\cliente\mantaeats\src\pages\list\list.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */]])
    ], ListPage);
    return ListPage;
    var ListPage_1;
}());

//# sourceMappingURL=list.js.map

/***/ }),

/***/ 85:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoginPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_auth_service__ = __webpack_require__(170);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var LoginPage = /** @class */ (function () {
    function LoginPage(navCtrl, navParams, viewCtrl, authService, alertCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.viewCtrl = viewCtrl;
        this.authService = authService;
        this.alertCtrl = alertCtrl;
    }
    LoginPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad LoginPage');
    };
    LoginPage.prototype.cancelar = function () {
        this.viewCtrl.dismiss();
    };
    LoginPage.prototype.logInFacebook = function () {
        var _this = this;
        this.authService.loginConFacebook().then(function (response) {
            var alert = _this.alertCtrl.create({
                title: 'Iniciar sesión',
                subTitle: 'Bienvenido, ' + response.user.displayName + '.',
                buttons: ['Aceptar']
            });
            alert.present();
            localStorage.setItem('loginData', JSON.stringify(response));
            _this.viewCtrl.dismiss();
        }).catch(function (err) {
            var alert = _this.alertCtrl.create({
                title: 'Operación no realizada',
                subTitle: 'Ocurrió un error al intentar iniciar sesión.',
                buttons: ['Aceptar']
            });
            alert.present();
            _this.viewCtrl.dismiss();
        });
    };
    LoginPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-login',template:/*ion-inline-start:"C:\Users\Jesucristhorr\Documents\proyectoProgramacionWeb\cliente\mantaeats\src\pages\login\login.html"*/'<!--\n  Generated template for the LoginPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header color="danger">\n\n  <ion-toolbar color="danger">\n    <ion-item color="danger">\n      <button ion-button clear (click)="cancelar()" item-start icon-only><ion-icon name="arrow-back" style="color: #ffffff"></ion-icon></button>\n      <ion-title text-center>Inicio de sesión</ion-title>\n    </ion-item>\n  </ion-toolbar>\n  \n\n</ion-header>\n\n\n<ion-content padding>\n  <button ion-button block color="blu" (click)="logInFacebook()" text-center>Inicio de sesión con Facebook</button>\n\n</ion-content>\n'/*ion-inline-end:"C:\Users\Jesucristhorr\Documents\proyectoProgramacionWeb\cliente\mantaeats\src\pages\login\login.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* ViewController */], __WEBPACK_IMPORTED_MODULE_2__services_auth_service__["a" /* AuthService */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */]])
    ], LoginPage);
    return LoginPage;
}());

//# sourceMappingURL=login.js.map

/***/ }),

/***/ 86:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PerfilPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_auth_service__ = __webpack_require__(170);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__login_login__ = __webpack_require__(85);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/**
 * Generated class for the PerfilPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var PerfilPage = /** @class */ (function () {
    function PerfilPage(navCtrl, navParams, authService, alertCtrl, viewCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.authService = authService;
        this.alertCtrl = alertCtrl;
        this.viewCtrl = viewCtrl;
        this.flag = false;
        this.anotherFlag = true;
        this.nombreUsuario = '';
        this.emailUsuario = '';
    }
    PerfilPage.prototype.ionViewWillEnter = function () {
        this.verificarSesion();
        if (localStorage.getItem('loginData') != null) {
            this.nombreUsuario = JSON.parse(localStorage.getItem('loginData')).user.displayName;
            this.emailUsuario = JSON.parse(localStorage.getItem('loginData')).user.email;
        }
    };
    PerfilPage.prototype.cerrarSesion = function () {
        var _this = this;
        if (localStorage.getItem('loginData') != null) {
            var confirm_1 = this.alertCtrl.create({
                title: 'Confirmación',
                message: '¿Estás seguro de que deseas cerrar sesión?',
                buttons: [
                    {
                        text: 'Cancelar',
                        handler: function () {
                            console.log('Cancelar clicked');
                        }
                    },
                    {
                        text: 'Aceptar',
                        handler: function () {
                            console.log('Aceptar clicked');
                            _this.authService.logout().then(function (result) {
                                var alert = _this.alertCtrl.create({
                                    title: 'Cerrar sesión',
                                    subTitle: 'Hasta pronto, ' + JSON.parse(localStorage.loginData).user.displayName + '.',
                                    buttons: ['Aceptar']
                                });
                                alert.present();
                                localStorage.removeItem('loginData');
                                if (_this.navCtrl.canGoBack()) {
                                    _this.viewCtrl.dismiss();
                                }
                                _this.verificarSesion();
                            }).catch(function (err) {
                                var alert = _this.alertCtrl.create({
                                    title: 'Operación fallida',
                                    subTitle: 'Ocurrió un error al tratar de cerrar sesión.',
                                    buttons: ['Aceptar']
                                });
                                alert.present();
                            });
                        }
                    }
                ]
            });
            confirm_1.present();
        }
        else {
            var alert_1 = this.alertCtrl.create({
                title: 'Lo sentimos',
                subTitle: 'No se puede cerrar sesión si no has iniciado sesión aún.',
                buttons: ['Aceptar']
            });
            alert_1.present();
        }
    };
    PerfilPage.prototype.verificarSesion = function () {
        var _this = this;
        setTimeout(function () {
            _this.imagenPerfil = document.getElementById('imagenProfile');
            if (localStorage.getItem('loginData') != null) {
                _this.flag = true;
                _this.anotherFlag = false;
                var rutaImagen = JSON.parse(localStorage.loginData).user.photoURL;
                _this.imagenPerfil.setAttribute("src", rutaImagen);
            }
            else {
                _this.flag = false;
                _this.anotherFlag = true;
            }
        }, 1);
    };
    PerfilPage.prototype.goToLogIn = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__login_login__["a" /* LoginPage */]);
    };
    PerfilPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-perfil',template:/*ion-inline-start:"C:\Users\Jesucristhorr\Documents\proyectoProgramacionWeb\cliente\mantaeats\src\pages\perfil\perfil.html"*/'<!--\n  Generated template for the PerfilPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar color="danger">\n    <ion-title>Perfil</ion-title>\n    <button ion-button menuToggle icon-only start>\n      <ion-icon name=\'information-circle\'></ion-icon>\n    </button>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n  <div id="container" [hidden]="!flag">\n    <img src="" id="imagenProfile" padding>\n    <p style="font-size: 16px; padding-bottom: 10px">Nombre: {{nombreUsuario}}</p>\n    <p style="font-size: 16px; padding-bottom: 10px">Email: {{emailUsuario}}</p>\n    <button ion-button block color="red" (click)="cerrarSesion()">Cerrar sesión</button>\n  </div>\n  <div [hidden]="!anotherFlag">\n    <button ion-button block color="grey" (click)="goToLogIn()">Iniciar sesión</button>\n  </div>\n</ion-content>\n'/*ion-inline-end:"C:\Users\Jesucristhorr\Documents\proyectoProgramacionWeb\cliente\mantaeats\src\pages\perfil\perfil.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__services_auth_service__["a" /* AuthService */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* ViewController */]])
    ], PerfilPage);
    return PerfilPage;
}());

//# sourceMappingURL=perfil.js.map

/***/ })

},[343]);
//# sourceMappingURL=main.js.map