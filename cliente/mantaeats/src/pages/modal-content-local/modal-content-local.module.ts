import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ModalContentLocalPage } from './modal-content-local';

@NgModule({
  declarations: [
    ModalContentLocalPage,
  ],
  imports: [
    IonicPageModule.forChild(ModalContentLocalPage),
  ],
})
export class ModalContentLocalPageModule {}
