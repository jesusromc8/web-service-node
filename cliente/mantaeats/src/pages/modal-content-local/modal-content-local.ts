import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, AlertController } from 'ionic-angular';
import { LocalesService } from '../../services/locales.service';
import { FirebaseApp } from '@angular/fire';

/**
 * Generated class for the ModalContentLocalPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-modal-content-local',
  templateUrl: 'modal-content-local.html',
})
export class ModalContentLocalPage {
  local: any = {};
  file: File;

  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController, public localesService:LocalesService, public firebase: FirebaseApp, public alertCtrl: AlertController) {
    this.local = navParams.get('local');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ModalContentLocalPage');
  }

  public changeListener($event) : void {
    this.file = $event.target.files[0];
  }

  public subirImagenAFirebase(imagen, local) {
    console.log(local)
    if(imagen == null || String(local.nombre).trim().charAt(0) == "" || String(local.direccion).trim().charAt(0) == "" || String(local.likes).trim().charAt(0) == "" || String(local.followers).trim().charAt(0) == "") {
      const alert = this.alertCtrl.create({
        title: 'No se agregó el local',
        subTitle: 'Por favor, rellena todos los campos.',
        buttons: ['Entiendo']
      });
      alert.present();
    } else {
      local.id = Date.now();
      this.localesService.createLocal(local);
      let storageRef = this.firebase.storage().ref('/locales/' + local.id);
      this.localesService.imagenABase64(imagen, function(base64) {    
        storageRef.putString(base64, 'data_url');
      });
      const alert = this.alertCtrl.create({
        title: 'Se agregó el local correctamente',
        subTitle: 'Gracias por agregar un nuevo local.',
        buttons: ['Aceptar']
      });
      alert.present();
      setTimeout(() => this.navCtrl.pop(), 2000);
    }
  }

}
