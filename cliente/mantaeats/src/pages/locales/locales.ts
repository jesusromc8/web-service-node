import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, ModalController, AlertController, UrlSerializer } from 'ionic-angular';
import { ModalContentPage } from '../modal-content/modal-content';
import { ModalContentLocalPage } from '../modal-content-local/modal-content-local';
import { LocalesService } from '../../services/locales.service';
import { ModalContentLocalEditPage } from '../modal-content-local-edit/modal-content-local-edit';
import { FirebaseApp } from '@angular/fire';
import { LoginPage } from '../login/login';
import { PerfilPage } from '../perfil/perfil';

/**
 * Generated class for the LocalesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-locales',
  templateUrl: 'locales.html',
})
export class LocalesPage {
  imagen: any;
  locales: any = [];

  constructor(public navCtrl: NavController, public navParams: NavParams, public loadingCtrl: LoadingController, public modalCtrl: ModalController, public alertCtrl: AlertController, public localesService: LocalesService, public firebase: FirebaseApp) {
    this.verificarSesion();
    this.localesService.getLocales().valueChanges().subscribe((localesFB) => {
      this.locales = localesFB;
    })
  }

  ionViewWillEnter() {
    this.verificarSesion();
  }

  realizarPedido() {
    if(localStorage.getItem('loginData') != null) {
      this.navCtrl.push(ModalContentPage, {local: {}})
    } else {
      let modal = this.modalCtrl.create(LoginPage);
      modal.present();
    }
  }

  irAgregarLocal() {
    if(localStorage.getItem('loginData') != null) {
      if(JSON.parse(localStorage.loginData).user.uid == 'Ks0SVJI1QSg2m9ra1x1AsQkfpUl2') {
        this.navCtrl.push(ModalContentLocalPage, {local: {}})
      } else {
        const alert = this.alertCtrl.create({
          title: 'Lo sentimos',
          subTitle: 'No posees los permisos necesarios para agregar locales, contacta al administrador.',
          buttons: ['Entiendo']
        });
        alert.present();
      }
    } else {
      let modal = this.modalCtrl.create(LoginPage);
      modal.present();
    }
  }

  irEditarLocal(local) {
    if(localStorage.getItem('loginData') != null) {
      if(JSON.parse(localStorage.loginData).user.uid == 'Ks0SVJI1QSg2m9ra1x1AsQkfpUl2') {
        this.navCtrl.push(ModalContentLocalEditPage, {local: local})
      } else {
        const alert = this.alertCtrl.create({
          title: 'Lo sentimos',
          subTitle: 'No posees los permisos necesarios para editar locales, contacta al administrador.',
          buttons: ['Entiendo']
        });
        alert.present();
      }
    } else {
      let modal = this.modalCtrl.create(LoginPage);
      modal.present();
    }
  }

  eliminarLocal(local) {
    if(localStorage.getItem('loginData') != null) {
      if(JSON.parse(localStorage.loginData).user.uid == 'Ks0SVJI1QSg2m9ra1x1AsQkfpUl2') {
        const confirm = this.alertCtrl.create({
          title: 'Confirmación',
          message: '¿Estás seguro de que deseas eliminar este local?',
          buttons: [
            {
              text: 'Cancelar',
              handler: () => {
                console.log('Cancelar clicked');
              }
            },
            {
              text: 'Aceptar',
              handler: () => {
                console.log('Aceptar clicked');
                this.firebase.storage().ref('/locales/' + local.id).delete();
                this.localesService.deleteLocal(local);
              }
            }
          ]
        });
        confirm.present();
      } else {
        const alert = this.alertCtrl.create({
          title: 'Lo sentimos',
          subTitle: 'No posees los permisos necesarios para eliminar locales, contacta al administrador.',
          buttons: ['Entiendo']
        });
        alert.present();
      }
    } else {
      let modal = this.modalCtrl.create(LoginPage);
      modal.present();
    }
  }

  verificarSesion() {
    setTimeout(() => {
      this.imagen = document.getElementById('imagenLocal');
      if(localStorage.getItem('loginData') != null) {
        let rutaImagen = JSON.parse(localStorage.loginData).user.photoURL;
        this.imagen.setAttribute("src", rutaImagen);
      } else {
        this.imagen.setAttribute("src", "http://cdn.onlinewebfonts.com/svg/img_411680.png")
      }
    }, 1)
  }

  tocarImagen() {
    if(localStorage.getItem('loginData') != null) {
      this.navCtrl.push(PerfilPage)
    } else {
      this.navCtrl.push(LoginPage)
    }
  }
}
