import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, ViewController } from 'ionic-angular';
import { AuthService } from '../../services/auth.service';
import { LoginPage } from '../login/login';

/**
 * Generated class for the PerfilPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-perfil',
  templateUrl: 'perfil.html',
})
export class PerfilPage {
  flag: boolean = false;
  anotherFlag: boolean = true;
  imagenPerfil: any;
  nombreUsuario: String = '';
  emailUsuario: String = '';

  constructor(public navCtrl: NavController, public navParams: NavParams, public authService: AuthService, public alertCtrl: AlertController, public viewCtrl: ViewController) {
  }

  ionViewWillEnter() {
    this.verificarSesion();
    if(localStorage.getItem('loginData') != null) {
      this.nombreUsuario = JSON.parse(localStorage.getItem('loginData')).user.displayName;
      this.emailUsuario = JSON.parse(localStorage.getItem('loginData')).user.email;
    }
  }

  cerrarSesion() {
    if(localStorage.getItem('loginData') != null) {
      const confirm = this.alertCtrl.create({
        title: 'Confirmación',
        message: '¿Estás seguro de que deseas cerrar sesión?',
        buttons: [
          {
            text: 'Cancelar',
            handler: () => {
              console.log('Cancelar clicked');
            }
          },
          {
            text: 'Aceptar',
            handler: () => {
              console.log('Aceptar clicked');
              this.authService.logout().then(result => {
                const alert = this.alertCtrl.create({
                  title: 'Cerrar sesión',
                  subTitle: 'Hasta pronto, ' + JSON.parse(localStorage.loginData).user.displayName + '.',
                  buttons: ['Aceptar']
                });
                alert.present();
                localStorage.removeItem('loginData');
                if(this.navCtrl.canGoBack()) {
                  this.viewCtrl.dismiss();
                }
                this.verificarSesion();
              }).catch(err => {
                const alert = this.alertCtrl.create({
                  title: 'Operación fallida',
                  subTitle: 'Ocurrió un error al tratar de cerrar sesión.',
                  buttons: ['Aceptar']
                });
                alert.present();
              })
            }
          }
        ]
      });
      confirm.present();
    } else {
      const alert = this.alertCtrl.create({
        title: 'Lo sentimos',
        subTitle: 'No se puede cerrar sesión si no has iniciado sesión aún.',
        buttons: ['Aceptar']
      });
      alert.present();
    }
  }

  verificarSesion() {
    setTimeout(() => {
      this.imagenPerfil = document.getElementById('imagenProfile');
      if(localStorage.getItem('loginData') != null) {
        this.flag = true;
        this.anotherFlag = false;
        let rutaImagen = JSON.parse(localStorage.loginData).user.photoURL;
        this.imagenPerfil.setAttribute("src", rutaImagen)
      } else {
        this.flag = false;
        this.anotherFlag = true;
      }
    }, 1)
  }

  goToLogIn() {
    this.navCtrl.push(LoginPage);
  }

}
