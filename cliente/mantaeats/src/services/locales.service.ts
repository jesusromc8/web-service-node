import { Injectable } from "@angular/core";
import { AngularFireDatabase } from "@angular/fire/database";
import { FirebaseApp } from "@angular/fire";
import { AlertController } from "ionic-angular";

@Injectable()
export class LocalesService {
  constructor(public afDB: AngularFireDatabase, public firebase: FirebaseApp, public alertCtrl: AlertController) {

  }
  public getLocales() {
    return this.afDB.list('/locales/');
  }

  public getLocal(id) {
    return this.afDB.object('/locales/' + id);
  }

  public createLocal(local) {
    return this.afDB.database.ref('/locales/' + local.id).set(local);
  }

  public editLocal(local) {
    return this.afDB.database.ref('/locales/' + local.id).set(local);
  }

  public deleteLocal(local) {
    return this.afDB.database.ref('/locales/' + local.id).remove().then((result) => {
      const alert = this.alertCtrl.create({
        title: 'Operación exitosa',
        subTitle: 'El local fue eliminado correctamente.',
        buttons: ['Aceptar']
      });
      alert.present();
    });
  }

  public imagenABase64(img, callback) {
    let fileReader = new FileReader();
    fileReader.addEventListener('load', event => {
      callback(fileReader.result);
    });
    fileReader.readAsDataURL(img);
  }

}